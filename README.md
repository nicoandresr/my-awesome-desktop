# Clone
```sh
git clone thisrepo ~
```
# Usage
Install feh
```
sudo pacman -S feh
```
### Use it
```sh
feh --bg-fill Walls Miku any-img.png
```
